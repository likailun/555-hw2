package awsstorage;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.AttributeUpdate;

import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.QueryOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.QuerySpec;

import awsrobot.AwsRobot;

public class DynamoDBHandler {
	
	public static DynamoDB dynamoDB;
	public static Table robotTable;
	public static Table urlVisitedTable;
	public static Table linkedGraphTable;
	public static Table contentSeenTable;
	public static Table urlQueueTable;

	public DynamoDBHandler(AmazonDynamoDB client ) {
//		try {
//            credentialsProvider.getCredentials();
//        } catch (Exception e) {
//            throw new AmazonClientException(
//                    "Cannot load the credentials from the credential profiles file. " +
//                    "Please make sure that your credentials file is at the correct " +
//                    "location (/home/vagrant/.aws/credentials), and is in valid format.",
//                    e);
//        }
//        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
//            .withCredentials(credentialsProvider)
//            .withRegion("us-east-1")
//            .build();

       dynamoDB = new DynamoDB(client);
       robotTable = dynamoDB.getTable("cis555-21a-9-robot");
       urlVisitedTable = dynamoDB.getTable("cis555-21a-9-urlVisited");
       linkedGraphTable = dynamoDB.getTable("cis555-21a-9-linkedGraph");
       contentSeenTable = dynamoDB.getTable("cis555-21a-9-contentSeen");
       urlQueueTable = dynamoDB.getTable("cis555-21a-9-urlQueue");
	}
	
	
	public static void addUrlVisited(String url) {
        try {
            System.out.println("Adding a new url visisted " + url);
            PutItemOutcome outcome = urlVisitedTable
	                .putItem(new Item().withPrimaryKey("url", url));

            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());            
        }
        catch (Exception e) {
            System.err.println("Unable to add urlVisited: " + url);
            System.err.println(e.getMessage());
        }
	}
	
	public static boolean ifUrlVisited(String url) {
        
        try {
            Item item = urlVisitedTable.getItem("url", url);
            if(item==null) {
            	return false;
            } 
            else {
            	return true;
            }
        }
        catch (Exception e) {
            System.err.println("Unable to search for visitedUrl " + url);
            System.err.println(e.getMessage());
        }
		return false;
	}
	
	public static String hashContent(String pass) {
		MessageDigest md;
		
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(pass.getBytes());
			byte[] digest = md.digest();  
			String hashed = new String(digest, StandardCharsets.UTF_8);
//		    System.out.println("BerkeleyInterface: HashedPassword:" + hashed); 
		    return hashed;

		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}
	
	public static void addContentSeen( String url, String htmlFile) {
        try {
            System.out.println("Adding a new contentseen " + url);
            String hashedContent = hashContent(htmlFile);
            PutItemOutcome outcome = contentSeenTable
	                .putItem(new Item().withPrimaryKey("content", hashedContent));
            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());            
        }
        catch (Exception e) {
            System.err.println("Unable to add contentseen: " + url);
            System.err.println(e.getMessage());
        }
	}
	
	public static boolean ifContentSeen(String url, String htmlFile) {
        
        try {
        	String hashedContent = hashContent(htmlFile);
            Item item = contentSeenTable.getItem("content", hashedContent);
            if(item==null) {
            	return false;
            } 
            else {
            	return true;
            }
        }
        catch (Exception e) {
            System.err.println("Unable to search for contentSeen " + url);
            System.err.println(e.getMessage());
        }
		return false;
	}
	
	public static void addLinkToLinkGraph( String startUrl, String endUrl) {
        try {
//            System.out.println("Adding a new link " + startUrl + " ----> " + endUrl);
            PutItemOutcome outcome = linkedGraphTable
	                .putItem(new Item().withPrimaryKey("startUrl", startUrl, "endUrl", endUrl));
//            System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());            
        }
        catch (Exception e) {
        	System.err.println("Adding a new link failed " + startUrl + " ----> " + endUrl);
        	System.err.println(e.getMessage());
        }
	}
	
	public static AwsRobot addRobot( String hostUrl, int crawlDelay, ArrayList<String> disallowLinks) {
        try {
        	int crawlDelayMilSec = crawlDelay * 1000;
        	long lastAccess = System.currentTimeMillis();
            System.out.println("Adding a robot " + hostUrl);
            Item item = new Item()
            		   .withPrimaryKey("hostUrl", hostUrl) 
            		   .withNumber("crawlDelay", crawlDelayMilSec) 
            		   .withLong("lastAccess", lastAccess) 
            		   .withList("disallowLinks", disallowLinks);
            PutItemOutcome outcome = robotTable
	                .putItem(item);
            System.out.println("Add Robot succeeded:\n" + outcome.getPutItemResult());  
            AwsRobot robot = new AwsRobot( hostUrl,  crawlDelay, disallowLinks, lastAccess);
            return robot;
        }
        catch (Exception e) {
        	System.err.println("Adding a new robot failed " + hostUrl);
        	System.err.println(e.getMessage());
        	return null;
        }
	}
	
	public static AwsRobot getRobot(String hostUrl) {
        
        try {
            Item item = robotTable.getItem("hostUrl", hostUrl);
            if(item==null) {
            	return null;
            } 
            else {
            	int crawlDelay = item.getInt("crawlDelay");
            	ArrayList<String> disallowLinks = (ArrayList<String>) item.get("disallowLinks");
            	long lastAccess = item.getLong("lastAccess");
            	AwsRobot robot = new AwsRobot( hostUrl,  crawlDelay, disallowLinks, lastAccess);
            	return robot;
            }
        }
        catch (Exception e) {
            System.err.println("Unable to get robot " + hostUrl);
            System.err.println(e.getMessage());
            return null;
        }
	}
	
public static boolean updateRobotAccessTime(String hostUrl) {
        
        try {
//        	robotTable.updateItem("hostUrl", hostUrl, "lastAccess");
        	long lastAccess = System.currentTimeMillis();
        	robotTable.updateItem(new PrimaryKey("hostUrl", hostUrl), 
        			new AttributeUpdate("lastAccess").addNumeric(lastAccess));
        	
        }
        catch (Exception e) {
            System.err.println("Unable to update robot " + hostUrl);
            System.err.println(e.getMessage());
        }
		return false;
	}

	public static void addUrlToQueue(String url) {
	    try {
	        System.out.println("Adding a url to queue " + url);
	        PutItemOutcome outcome = urlQueueTable
	                .putItem(new Item().withPrimaryKey("url", url));
	
//	        System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());            
	    }
	    catch (Exception e) {
	        System.err.println("Unable to add urlVisited: " + url);
	        System.err.println(e.getMessage());
	    }
	}

//public static void main(String[] args) {
//	ProfileCredentialsProvider credentialsProvider = new ProfileCredentialsProvider("cis555 final");
//	DynamoDBHandler dynamoDBHandler = new DynamoDBHandler(credentialsProvider);
////	ArrayList<String> linksdisallow = new ArrayList<String>();
////	linksdisallow.add("/asdfasdf");
////	linksdisallow.add("/21312312");
////	dynamoDBHandler.addRobot("www.cnn.com/", 5, linksdisallow);
////	dynamoDBHandler.updateRobotAccessTime("www.cnn.com/");
//	dynamoDBHandler.getRobot("www.cnn.com/");
//
//	
//	
//}
	
	
	
	


}
