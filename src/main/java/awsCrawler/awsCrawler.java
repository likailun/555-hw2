package awsCrawler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.auth.BasicAWSCredentials;
import awsrobot.AwsRobot;
import awsrobot.AwsRobotHandler;
import awsstorage.DynamoDBHandler;
import awsstorage.S3Handler;



public class awsCrawler {
    // pool of CrawlerWorkers etc.
	Logger logger = LogManager.getLogger(awsCrawler.class);
    // data structure
    // url queue
    Queue<String> urlQueue = new LinkedList<>();
//    Queue<String> urlVisited = new LinkedList<>();
//	public static Map<String, Robot> RobotMap = new LinkedHashMap<String, Robot>();

    // url visited
    // content seen 
    public String startUrl;
//    public StorageInterface db;
    public int maxFileSize;
    public int maxNumberDocuments;
    public int countDocuments = 0;
    public Set<String> acceptContentType =  
    		new HashSet<String>(Arrays.asList(new String[] {"text/html"}));
   public String realBaseUrl;
   public long lastModified;
   public String contentType;
   private static final String USER_AGENT = "cis455crawler";

   public DynamoDBHandler dynamoDBHandler;
   public S3Handler	s3Handler;
//   public ProfileCredentialsProvider credentialsProvider;
   public AWSStaticCredentialsProvider credentialsProvider;
   private String accessKey = "AKIAQ5J6G6H3OSNTHF6M";
   private String secretKey = "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";
//	private static final String POST_URL = "https://localhost:9090/SpringMVCExample/home";
//
//	private static final String POST_PARAMS = "userName=Pankaj";
	
    public awsCrawler(String startUrl, int size, int count) {
        this.startUrl = startUrl;
        this.maxFileSize = size*1000000;   //convert to bytes
        this.maxNumberDocuments = count;
//        credentialsProvider = new ProfileCredentialsProvider("cis555 final");
        credentialsProvider = new AWSStaticCredentialsProvider(
        		new BasicAWSCredentials(accessKey, secretKey));
		
		try {
            credentialsProvider.getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (/home/vagrant/.aws/credentials), and is in valid format.",
                    e);
        }
		AmazonS3 s3 = AmazonS3ClientBuilder.standard()
	            .withCredentials(credentialsProvider)
	            .withRegion("us-east-1")
	            .build();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard()
	            .withCredentials(credentialsProvider)
	            .withRegion("us-east-1")
	            .build();
		dynamoDBHandler = new DynamoDBHandler(client);
		s3Handler = new S3Handler(s3);
    }
    
    public AwsRobot robotMapper(String url, String host) {
		try {
			AwsRobot robot = dynamoDBHandler.getRobot(host);
			if (robot==null) {
				//create robot
				robot = new AwsRobotHandler(url, dynamoDBHandler).createNewRobot();
				return robot;
			}
			else {
				return robot;
			}
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    public String getRealUrl(String inputUrl) {
    	URL obj;
		try {
		obj = new URL(inputUrl);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setInstanceFollowRedirects(false);
		con.connect();
		String locationAttribute = con.getHeaderField("Location");
		//if no redirect
		if (locationAttribute==null) {
			realBaseUrl = con.getURL().toString();
			return realBaseUrl;
		}
		realBaseUrl = con.getHeaderField("Location").toString();
		return realBaseUrl;
		}
		
		catch(IOException e) {
			return null;
		}
    }
    
    public String sendGet(String inputUrl) {
    	URL obj;
		try {
			obj = new URL(inputUrl);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int code = con.getResponseCode();		
		if (code == HttpURLConnection.HTTP_OK) { // success
			InputStream stream = con.getInputStream();
			String contents = new String(stream.readAllBytes(), StandardCharsets.UTF_8);
			System.out.println("Get Request Successfully Sent to " + inputUrl);
			return contents;
		} else {
			System.err.println("GET request not worked");
			return null;
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		
    }
    
    public void savePageLinkToFile(String inputLink, String outputLink) {
		try {
	        
	        File txtFile = new File("pagelink.txt");
		    if(txtFile.exists()==false){
	            txtFile.createNewFile();
	    }
//    		System.out.println("new page link " + inputLink + "---> " + outputLink );
			PrintWriter out = new PrintWriter(new FileWriter(txtFile, true));
			out.append("("+inputLink + "," + outputLink + ")" +"\n");
			out.close();

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public Boolean sendHead(String inputUrl)  {
    	URL obj;
	try {
			obj = new URL(inputUrl);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("HEAD");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int code = con.getResponseCode();
		if (code == HttpURLConnection.HTTP_OK) {
			InputStream is = con.getInputStream();
			realBaseUrl = con.getURL().toString();
			contentType = con.getContentType();
			int contentLength = con.getContentLength();
			lastModified = con.getLastModified();

			if (contentType.contains("text/html") && contentLength < maxFileSize) {
				return true;
			}
			else {
				System.err.println("Wrong content type or content length " + inputUrl);
				return false;
			}			
		} else {
			System.out.println("HEAD Request Failed");
			return false;
		}
		
	} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
    }    

    /**
     * Main thread
     * @throws Exception 
     */
    public void start() throws Exception {
    	//add start url to the queue to start
    	urlQueue.add(startUrl);
    	
    	while(!urlQueue.isEmpty() && countDocuments < maxNumberDocuments ) {
        	
        	try {
        	lastModified = 0;
        	boolean needUpdate = false;
        	long oldLastModified = 0;
        	String htmlFile = null;
    		String currentUrl = urlQueue.remove();
    		int currentCrawlDelay = 0;
    		String hosturl = "";
    		
    		
    		if(dynamoDBHandler.ifUrlVisited(currentUrl)) {
    			//break jump to the next url if this url is visited
    			logger.error("Url visited already " + currentUrl);
    			continue;
    		}
    		
			URL urlObj = new URL(currentUrl);
			hosturl = urlObj.getHost();
    		AwsRobot currentRobot = robotMapper(currentUrl, hosturl);
    		if(currentRobot == null) {continue;}
//    		Robot currentRobot = RobotMap.get(host);
    		currentRobot = null;
    		if(currentRobot!=null) {
        		currentCrawlDelay = currentRobot.getCrawlDelay();
    			if(!currentRobot.isTimeAllowed()) {
    				// re add the url to queue and skil to next url
    				urlQueue.add(currentUrl);
//    				logger.error("Robot Time not Allowed " + currentUrl );
    				continue;
    			}
    		}
    		
    		boolean fileIsValid = sendHead(currentUrl);
    		if(currentRobot!=null) {
    		dynamoDBHandler.updateRobotAccessTime(hosturl);}

    		System.out.println("Getting Real Url " + realBaseUrl );
    		if(realBaseUrl == null) {
    			dynamoDBHandler.addUrlVisited(currentUrl);
    			continue;
    		}
    		
    		currentUrl = realBaseUrl;
    		
    		if(dynamoDBHandler.ifUrlVisited(currentUrl)) {
    			//break jump to the next url if this url is visited
    			logger.error("Real Url visited already " + currentUrl);
    			continue;
    		}
    		
    		if(currentRobot!=null) {
    			if(!currentRobot.isPathAllowed(currentUrl)) {
    				//path not allowed, add to visited, dont come next time
    				logger.error("Robot Path not Allowed " + currentUrl );
    	    		dynamoDBHandler.addUrlVisited(currentUrl);
    				continue;
    			}
    		}
    		
//    		urlVisited.add(currentUrl);
    		dynamoDBHandler.addUrlVisited(currentUrl);
    		System.out.println("Currently process " +  currentUrl);
    		
    		
    		if (!fileIsValid) {
    			logger.error("Url file is not valid, content type or content length " + currentUrl );
    			continue;
    		}

    		// download htmlfile
			Thread.sleep(currentCrawlDelay);
			htmlFile = sendGet(currentUrl);
			
			if(currentRobot!=null) {
				dynamoDBHandler.updateRobotAccessTime(hosturl);}
			logger.info(currentUrl + ": downloading");
		
    		//else use the html file from the db
    		
    		if (htmlFile == null) {
    			logger.error("URL Request Failed " + currentUrl);
    			continue;
    		}
    		
    		// check content seen
//    		String contentSeenUrl = db.getContentSeen(htmlFile);
    		// if already seen the same content
    		if (dynamoDBHandler.ifContentSeen(currentUrl, htmlFile)) {
    			logger.error("Seen the same content already " + currentUrl);
    			continue;
    		}
    		
//    		if (contentSeenUrl!= null) {
//    			logger.error("Seen the same content already " + currentUrl 
//    					+ " and " + contentSeenUrl);
//    			continue;
//    		}
    		
    		// save to s3
    		s3Handler.addHtmlFileToS3(currentUrl, htmlFile);

//    		db.addContentSeen(htmlFile, currentUrl);
    		dynamoDBHandler.addContentSeen(currentUrl, htmlFile);
    		incCount();

    		
    		
    		Document doc = Jsoup.parse(htmlFile);
    		Elements linksOnPage = doc.getElementsByTag("a");
            for (Element page : linksOnPage) {
            	String extractedUrl = page.attr("href");
            	String finalUrl = null;
            	URI u = new URI(extractedUrl);
            	if(u.isAbsolute()) {
            		finalUrl = extractedUrl;
            	}
            	else {
            		URL newFinalurl = new URL(new URL(realBaseUrl), u.toString());
            		finalUrl = newFinalurl.toString();
            	}
            	
            	urlQueue.add(finalUrl);
            	savePageLinkToFile(realBaseUrl, finalUrl);
            	dynamoDBHandler.addLinkToLinkGraph(realBaseUrl, finalUrl);
            }
            
        	}
        	catch (Exception e){
        		e.printStackTrace();
        		continue;
        	}
    		
    		
    	}
    	
    }

    /**
     * We've indexed another document
     */
    public void incCount() {
    	countDocuments+=1;
    }

    /**
     * Main program: init database, start crawler, wait for it to notify that it is
     * done, then close.
     * @throws Exception 
     */
    public static void main(String args[]) throws Exception {

        System.out.println("Crawler starting");
        String startUrl = args[0];
        Integer size = 20;
        Integer count = Integer.valueOf(args[1]);


        awsCrawler crawler = new awsCrawler(startUrl, size, count);
        System.out.println("Starting crawl of " + count + " documents, starting at " + startUrl);
        crawler.start();

        System.out.println("Done crawling!");
        System.out.println("DB closed!");
    }

}

