package awsrobot;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import awsstorage.DynamoDBHandler;

public class AwsRobotHandler {
	String inputUrl;
	ArrayList<String> disallowLinks = new ArrayList<String>();

	String host;
	int crawlDelay = 0;
	DynamoDBHandler dynamoDBHandler;
	
	public AwsRobotHandler(String url, DynamoDBHandler dynamoDBHandler) {
		this.inputUrl = url;
		this.dynamoDBHandler = dynamoDBHandler;
	}
	
	
	public AwsRobot createNewRobot() {
		try {
			URL inputurl = new URL(inputUrl);
			host = inputurl.getHost();
			String protocal = inputurl.getProtocol();
			String robotLink = protocal + "://" + host + "/robots.txt";
			BufferedReader in = sendGet(robotLink);
			if(in==null) {
				return null;
			}
			parser(in);
			AwsRobot newlyCreated = dynamoDBHandler.addRobot(host, crawlDelay, disallowLinks);			
			return newlyCreated;
			
			
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	
    public BufferedReader sendGet(String inputUrl) {
    	URL obj;
		try {
			obj = new URL(inputUrl);
		
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", "cis455crawler");
		int code = con.getResponseCode();		
		if (code == HttpURLConnection.HTTP_OK) { // success
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			return in;
		} else {
			System.err.println("GET request not worked");
			return null;
		}
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    public void subparser(String[] eachSection) {
		for (String s: eachSection) {       
			String[] lineList = s.split(":");
		if (lineList.length!=2) {
			continue;
		}

		if (lineList[0].strip().equals("Disallow")) {
			String eachDisallow = lineList[1].strip();
			disallowLinks.add(eachDisallow);
		}
		
		if (lineList[0].strip().equals("Crawl-delay")) {
			crawlDelay = Integer.parseInt(lineList[1].strip());
			}
		    
		}
    }
    
    
    public void parser(BufferedReader in) {
		try {
			String line;
			StringBuffer response = new StringBuffer();
			while ((line = in.readLine()) != null) {
				if(line.equals("")) {
					response.append("break");
				}
				else {
					response.append(line+"lineline");
				}
				
				}
			String robotRaw = response.toString();
			String[] lineList = robotRaw.split("break");
		
			
			// if contains cis455, find it and only parse that 
			if (robotRaw.contains("cis455crawler")){
			for (String s: lineList) {           
			    //Do your stuff here
				if (s.contains("cis455crawler")){
					String[] eachSection = s.split("lineline");
					subparser(eachSection);
					return;
				}
			}
			}
			// if not contains, only parse the first one
			else {
				String s = lineList[0];
				String[] eachSection = s.split("lineline");
				subparser(eachSection);
				return;
			}

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	
	
	
}
