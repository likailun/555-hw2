package awsrobot;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class AwsRobot {
	int crawlDelay=0;
	ArrayList<String> disallowLinks;
	String host;
	long lastAccess;

	public AwsRobot(String host, int crawlDelay, ArrayList<String> disallowLinks, long lastAccess) {
		this.crawlDelay = crawlDelay;  //second to mil
		this.host = host;
		this.disallowLinks = disallowLinks;
		this.lastAccess =  lastAccess;
	}
	
	public int getCrawlDelay() {
		return crawlDelay;
	}
	
	public boolean isPathAllowed(String url) {
		try {
			URL urlObj = new URL(url);
			String host = urlObj.getHost();
			String protocal = urlObj.getProtocol();
			//allow everything
			if(disallowLinks.size()==0) {
				return true;
			}
			
			for(String disallowlink : disallowLinks) {
				String disallowUrl = protocal + "://" + host + disallowlink;
				
				// dont allow anyone's access
				if(disallowlink.equals("/")) {
					return false;
				}
				
				if (disallowlink.endsWith("/")) {
					if(url.startsWith(disallowUrl)) {
						return false;
					}
				}
				else {
					// if a file disallowed
					if(disallowUrl.equals(url)) {
						return false;
					}
				}
			
		}
			return true;
			
		}catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}
	
//	public void updateAccessTime() {
//		lastAccess = System.currentTimeMillis();
//	}
	
	public boolean isTimeAllowed() {
		long expirationTime = lastAccess + crawlDelay;
		long currentTime = System.currentTimeMillis();
		if (currentTime > expirationTime) {
			return true;
		}
		else {
			return false;
		}
		
	}


}
